import React, {useEffect, useState} from 'react'
import {Flex, Navbar, Wrapper, Paper} from './styles'
import DeleteIcon from '@material-ui/icons/Delete';

const MyPokemon = () => {
    var array = JSON.parse(localStorage.getItem('pokemon_data'))
    const [list, setList] = useState([])
    const handleDelete = (id) => {
        const newList = list.filter((item) => item.id !== id);
        setList(newList);
        localStorage.setItem('pokemon_data', JSON.stringify(newList))
    }
    const getLocal = () => {
        if(localStorage.getItem('pokemon_data') === null){
            localStorage.setItem('pokemon_data', JSON.stringify([]))
        }
        let pokeLocal = JSON.parse(localStorage.getItem('pokemon_data'))
        setList(pokeLocal)
    }
    useEffect(() => {
        getLocal()
    }, [])
    return(
        <div>
            <Navbar>
                <Flex direction="row" justify="space-between">
                    <h1>
                        <a href="/">Pokedex</a>
                    </h1>
                    <h3>
                        <a href="/my-pokemon">My Pokemon</a>
                    </h3>    
                </Flex>
            </Navbar>
            <Wrapper>
                <Flex direction="row" justify="center">
                    <h1>My Pokemon</h1>
                </Flex>
                <Flex direction="row" justify="space-around" wrap="wrap">
                    {list.map(items => (
                        <Paper>
                            <Flex direction="row" justify="space-between">
                                <h1>#{items.id}</h1>
                                <DeleteIcon fontSize="large" onClick={() => handleDelete(items.id)} style={{color: 'white', marginRight: '1em'}} />
                            </Flex>
                            <Flex direction="row" justify="flex-start">
                                <Flex direction="column" justify="flex-start">
                                    <h3>Pokemon: {items.poke_name}</h3>
                                    <h5>Name: {items.name}</h5>
                                </Flex>
                            </Flex>
                        </Paper>
                    ))}
                </Flex>
            </Wrapper>
        </div>
    )
}

export default MyPokemon