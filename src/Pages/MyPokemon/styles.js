import styled from 'styled-components'

export const Flex = styled.div`
    display: flex;
    flex-direction: ${({direction}) => direction};
    justify-content: ${({justify}) => justify};
    align-items: ${({alignItems}) => alignItems};
    flex-wrap: ${({wrap}) => wrap};
    width: 100%;
    // height: 100%;
`;

export const Navbar = styled.div`
    width: 100%;
    background: #284089;
    color: white;
    padding-top: 2em;
    padding-bottom: 1em;
    h1{
        margin-left: 1em;
    }
    a{
        margin-right: 1em;
        color: white;
        text-decoration: none;
    }
    @media (max-width: 414px){
        a{
            margin-top: 1em;
        }
    }
`

export const Wrapper = styled.div`
    margin-top: 2em;
`

export const Paper = styled.div`
    width: 22em;
    height: 10em;
    padding-left: 2em;
    padding-top: 1em;
    border-radius: 10px;
    margin-top: 1em;
    background: #d81b60;
    // background: ${({background}) => background};
    filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));
    img{
        width: 8em;
    }
    h1{
        text-transform: capitalize;
        color: white;
        font-weight: 600;
        // padding-bottom: 5px;
        margin: 0;
    }

    @media (max-width: 414px){
        width: 22em;
    }
`