import React, {useState, useEffect, useRef} from 'react'
import axios from 'axios'
import {Flex, Navbar, Wrapper, Paper} from './styles'
import {Button} from '@material-ui/core'
import Details from '../Details'

const Home = () => {
    const [items, setItems] = useState([])
    const [values, setValues] = useState([])
    const [open, setOpen] = useState(false)
    
    const handleColor = (type) => {
        if(type == "normal"){
            return "#e57373"
        }
        if(type == "grass"){
            return "#009688";
        }
        if(type == "fire"){
            return "#ef5350";
        }
        if(type == "water"){
            return "#3f51b5"
        }
        if(type == "electric"){
            return "#ffeb3b"
        }
        if(type == "ice"){
            return "#42a5f5"
        }
        if(type == "fighting"){
            return "#795548"
        }
        if(type == "poison"){
            return "7e57c2"
        }
        if(type == "ground"){
            return "#fbc02d"
        }
        if(type == "flying"){
            return "#7e57c2"
        }
        if(type == "psychic"){
            return "#ec407a"
        }
        if(type == "bug"){
            return "#26a69a"
        }
        return "white"
    }
    // const randomInteger = (min, max) => { 
    //     return Math.floor(Math.random() * (max - min + 1) + min)
    // }

    const handleClick = (id) => {
        console.log(id)
        axios.get(`https://pokeapi.co/api/v2/pokemon/${id}`)
        .then(res => {
            const values = [res.data]
            console.log([res.data])
            setOpen(true)
            setValues(values)
        })
    }
    // const handleCatch = () => {
    //     const rndInt = randomInteger(0, 1)
    //     if(rndInt == 0){
    //         setCatched(false)
    //     }
    //     else{
    //         setCatched(true)
    //     }
    //     console.log(rndInt)
    // }
    if(open){
        document.body.style.overflow = 'hidden'
    }
    else{
        document.body.style.overflow = 'scroll'
    }
    
    useEffect(() => {
        axios.get(`https://pokeapi.co/api/v2/pokemon?limit=20`)
        .then(res => {
            const items = res.data.results
            const spesificItems = Array.from(items, x => x.url)
            Promise.all(spesificItems.map(y => axios.get(y)))
            .then(responses => {
                setItems(responses)
                console.log(responses)
            })
        })
        
    },[])

    
    return(
        
        <div>
            <Navbar>
                <Flex direction="row" justify="space-between">
                    <h1>Pokedex</h1>
                    <h3>
                        <a href="/my-pokemon">My Pokemon</a>
                    </h3>    
                </Flex>
            </Navbar>
            <Wrapper>

                <Flex direction="row" justify="space-around" wrap="wrap">
                    {items.map(items => (   
                        <Paper background={handleColor(items.data.types[0].type.name)} onClick={() => handleClick(items.data.id)}>
                             <Flex direction="column" justify="center" alignItems="center">
                                <img src={items.data.sprites.other.dream_world.front_default} alt="poke_pic"/>
                                <div style={{textAlign: 'center'}}>
                                    <p>{items.data.name}</p>
                                    <p>Type: {items.data.types[0].type.name}</p>
                                </div>
                                
                             </Flex>
                         </Paper>
                    ))}
                    {open && (
                        values.map((values, i) => (    
                            <Details key={i} 
                                id={values.id}
                                name={values.name}
                                background={handleColor(values.types[0].type.name)}
                                image={values.sprites.other.dream_world.front_default}
                                type={values.types[0].type.name}
                                ability={values.abilities[0].ability.name}
                                moves={values.moves[0].move.name}
                                // handleCatch={handleCatch}
                                handleClose={() => setOpen(false)}
                                animation={open ? "Slide_Right 1.4s ease" : "Slide_Right 1.4s ease"}
                            />
                        ))
                    )}
                </Flex>
               
            </Wrapper>
            
        </div>
    )
}

export default Home