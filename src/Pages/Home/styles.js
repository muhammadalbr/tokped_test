import styled from 'styled-components'

export const Flex = styled.div`
    display: flex;
    flex-direction: ${({direction}) => direction};
    justify-content: ${({justify}) => justify};
    align-items: ${({alignItems}) => alignItems};
    flex-wrap: ${({wrap}) => wrap};
    width: 100%;
    height: 100%;
`;

export const Navbar = styled.div`
    width: 100%;
    background: #284089;
    color: white;
    padding-top: 2em;
    padding-bottom: 1em;
    h1{
        margin-left: 1em;
    }
    a{
        margin-right: 1em;
        color: white;
        text-decoration: none;
    }
    @media (max-width: 414px){
        a{
            margin-top: 1em;
        }
    }
`

export const Wrapper = styled.div`
    margin-top: 2em;
    padding-left: 5em;
    padding-right: 5em;
    // background: #424242;

    // .tester{
    //     position: absolute;
    //     top: 5em;
    //     animation: Slide_Up 1.4s ease;
    //     width: 800px;
    // }
    
    @media (max-width: 414px){
        padding: 0;
    }

    // @keyframes Slide_Up{
    //     0%{
    //         transform: translateY(250px)
    //     }
    //     50%{
    //         transform: translateY(100px)
    //     }
    //     100%{
    //         transform: translateY(0)
    //     }
    // }
    
    
`

export const Paper = styled.div`
    width: 15em;
    height: 15em;
    margin-top: 1em;
    background: ${({background}) => background};
    // background: white;
    border-radius: 10px;
    filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));
    img{
        width: 5em;
        height: 7em;
    }
    p{
        text-transform: capitalize;
        color: white;
        font-weight: 600;
        padding-bottom: 5px;
    }

    @media (max-width: 414px){
        width: 12em;
        height: 12em;
        img{
            width: 10em;
            height: 6em;
            padding-top: 15px;
        }
    }
    @media (max-width: 375px){
        width: 11em;
    }
`


