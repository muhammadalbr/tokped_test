import React, {useRef, useEffect, useState} from 'react'
import {Flex, Wrapper, Paper} from './styles'
import {Button, CircularProgress, TextField} from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close';

const Details = ({id, name, background, image, type, ability, moves, animation, handleClose}) => {
    const [catched, setCatched] = useState(null)
    const [loading, setLoading] = useState(false)
    const [value, setValue] = useState({
        id: '',
        name: '',
        poke_name: ''
    })
    const [list, setList] = useState([])
    const handleChange = (e) => {
        setValue({
            name: e.target.value
        })
    }
    const getLocal = () => {
        if(localStorage.getItem('pokemon_data') === null){
            localStorage.setItem('pokemon_data', JSON.stringify([]))
        }
        let pokeLocal = JSON.parse(localStorage.getItem('pokemon_data'))
        setList(pokeLocal)
    }

    const randomInteger = (min, max) => { 
        return Math.floor(Math.random() * (max - min + 1) + min)
    }
    const handleCatch = async() => {
        const rndInt = randomInteger(0, 1)
        if(rndInt == 0){
            setLoading(true)
            setTimeout(function() {
                setLoading(false)
                setCatched(false)

            }, 2000);
        }
        else{
            setLoading(true)
            setTimeout(function() {
                setLoading(false)
                setCatched(true)
            }, 2000);
        }
        console.log(rndInt)
    }
    const handleSubmit = () => {
        var arr = JSON.parse(localStorage.getItem('pokemon_data'))
        if(value.name === ''){
            alert('Please insert your poke name!')
            console.log(arr[0].id)
        }
        else{
            list.push({
                id: id,
                name: value.name,
                poke_name: name
            })
            setValue({
                name: ''
            })
            localStorage.setItem('pokemon_data', JSON.stringify(list))
        }
    }
    useEffect(() => {
        getLocal()
    }, [])
    return(
        <Paper background={background} animation={animation}>
            <Flex direction="row" justify="flex-end">
                    <CloseIcon onClick={handleClose} />
            </Flex>
            <Flex direction="row" style={{background}}>
                
                <Flex direction="column" alignItems="center">
                    <img src={image} alt="detail_pokepic"/>
                    <h1>{name}</h1>
                    <p className="type">{type}</p>
                    <Flex direction="row" justify="center" alignItems="center" style={{background: 'white'}}>
                        <Flex direction="row" justify="space-around" style={{height: '10em'}}>
                            <Flex direction="column" style={{marginLeft: '4em'}}>
                                <p><b>Abilities</b></p>
                                <p>{ability}</p>
                            </Flex>
                            <Flex direction="column">
                                <p><b>Moves</b></p>
                                <p>{moves}</p>
                            </Flex>
                        </Flex>
                        
                        
                    </Flex>
                    <Flex direction="row" justify="center" alignItems="center" style={{background: 'white'}}>
                        {loading ?
                            <CircularProgress />
                            :
                            <Button variant="contained" color="primary" onClick={handleCatch}>Catch</Button>
                        }
                        
                        
                    </Flex>
                    {catched == true ?
                            <Flex direction="column" justify="center" alignItems="center">
                                <p className="bottom">Yes you got it!, enter your poke name here</p>
                                <Flex direction="row" justify="space-around">
                                    <TextField 
                                        id="outlined-basic" 
                                        label="Outlined" 
                                        variant="outlined" 
                                        value={value.name}
                                        onChange={handleChange}
                                        style={{marginBottom: '1em'}} 
                                    />
                                    <Button variant="contained" color="primary" onClick={handleSubmit}>
                                        Submit
                                    </Button>
                                </Flex>
                            </Flex>
                            :
                            catched == false ?
                            <p className="bottom">Oops, try again!</p>
                            :
                            null
                        }
                </Flex>
            </Flex>
        </Paper>
    )
}

export default Details