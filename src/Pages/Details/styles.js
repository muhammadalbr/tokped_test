import styled from 'styled-components'

export const Flex = styled.div`
    display: flex;
    flex-direction: ${({direction}) => direction};
    justify-content: ${({justify}) => justify};
    align-items: ${({alignItems}) => alignItems};
    flex-wrap: ${({wrap}) => wrap};
    width: 100%;
    // height: 100%;
`;

export const Wrapper = styled.div`
    margin-top: 2em;
`

export const Paper = styled.div`
    width: 22em;
    height: 25em;
    // margin-top: 1em;
    position: fixed;
    background: ${({background}) => background};
    animation: ${({animation}) => animation};
    // animation: Slide_Up 1.4s ease;
    border-radius: 10px;
    filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));
    img{
        width: 8em;
    }
    h1{
        text-transform: capitalize;
        color: white;
        font-weight: 600;
        // padding-bottom: 5px;
        margin: 0;
    }
    .type{
        background: #cfd8dc;
        opacity: 0.8;
        border-radius: 30px;
        padding: 5px;
        margin-bottom: 10px;
    }
    p{
        font-size: 16px;
    }
    button{
        margin-bottom: 10px;
    }
    .bottom{
        color: white;
        font-weight: 700;
    }
    // @media (max-width: 414px){
    //     width: 12em;
    //     height: 12em;
    //     img{
    //         width: 10em;
    //         height: 6em;
    //         padding-top: 15px;
    //     }
    // }

    @keyframes Slide_Up{
        0%{
            transform: translateY(100px)
        }
        50%{
            transform: translateY(50px)
        }
        100%{
            transform: translateY(0)
        }
    }

    @keyframes Slide_Down{
        0%{
            transform: translateY(0)
        }
        50%{
            transform: translateY(100px)
        }
        100%{
            transform: translateY(2000px)
        }
    }

    @media (max-width: 414px){
        width: 25em;
        height: 10em;
        img{
            width: 20em;
            height: 8em;
        }
        .type{
            margin-bottom: 0px;
        }
        // button{
        //     margin-bottom: 10px;
        // }
        .bottom{
            color: white;
            font-weight: 700;
        }
        animation: ${({animation}) => animation};
        @keyframes Slide_Right{
            0%{
                transform: translateX(-200px)
                // margin-left: -300px;
            }
            50%{
                transform: translateX(-50px)
                // margin-left: -100px;
            }
            100%{
                transform: translateX(0)
                // margin-left: 60px;
            }
        }
        @keyframes Slide_Left{
            0%{
                transform: translateX(0)
                // margin-left: -300px;
            }
            50%{
                transform: translateX(-50px)
                // margin-left: -100px;
            }
            100%{
                transform: translateX(-200px)
                // margin-left: 60px;
            }
        }
    }
    @media (max-width: 414px){
        width: 22em;
    }
`