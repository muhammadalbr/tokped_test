import React from 'react'
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import Home from './Pages/Home'
import Pokemon from './Pages/MyPokemon'

const Router = () => {

    return(
        <div>
            <BrowserRouter>
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/my-pokemon" component={Pokemon} />
                </Switch>
            </BrowserRouter>
        </div>
    )
}

export default Router